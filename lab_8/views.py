from django.shortcuts import render

# Create your views here.
author = 'Khoirul Khuluqi Abdulloh'
def index(request):
    response = {'author': author}
    return render(request, 'lab_8/lab_8.html', response)
