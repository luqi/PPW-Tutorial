// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '244177042780646',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)

  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
  FB.getLoginStatus(function (response) {
      if (response.status === 'connected') {
          render(true);
      }
      else {
          render(false);
      }
  });
};

// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = (loginFlag) => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="profile">' +
          '<div class="cover"><img src="' + user.cover.source + '"/></div>' +
          '<div class="picture">' +
            '<div class="col-sm-3 pic">' + '<img src="' + user.picture.data.url + '" alt="profpic" />' + '</div>' +
            '<div class="name">' + user.name + '</div>' +
          '</div>' +
          '<div class="data">' +
            '<h2>' + user.about + '</h2>' +
            '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
          '</div>' +
          '<div id="logout-btn">' +
            '<button class="btn btn-danger logout" onclick="facebookLogout()">Logout</button>' +
          '</div>' +
        '</div>' +
        '<div id="feeds">' +
          '<div class="feed">' +
            '<form class="form-horizontal" role="form">' +
              '<div class="form-group" style="padding:14px;">' +
                '<textarea id="postInput" class="form-control" placeholder="Ketik Statusmu"></textarea>' +
              '</div>' +
              '<button class="btn btn-success pull-right" type="button" onclick="postStatus()">Post</button>' +
            '</form>' +
          '</div>' +
        '</div>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          var userID = value.id.split("_")[0];
          var messageId = value.id.split("_")[1];
          var html = '<div class="feed">'
          if (value.story) {
            html += '<h2>' + value.story + '</h2>'
          }
          if (value.message) {
            html += '<h1>' + value.message + '</h1>'
          }
          if (value.attachments) {
            value.attachments.data.map(attachment => {
              html += '<a href='+attachment.url+'>' + '<img style="margin-top:20px; margin-bottom:30px" src='+attachment.media.image.src+'> </a>'
            });
          }
          html += 
          '<div id="delete-button">' +
            '<button class="btn btn-danger" onclick="deletePost('+ userID + "," + messageId + ')">delete</button>' +
          '</div>';
          $('#feeds').append(
            html + '</div>'
          )
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html(
      '<div id="login-btn">' +
        '<button class="loginBtn loginBtn--facebook" onclick="facebookLogin()">Login with Facebook</button>' +
      '</div>'
    );
  }
};

const facebookLogin = () => {
  FB.login(function(response){
     console.log(response);
     render(true);
   }, {scope:'email,public_profile,user_posts,publish_actions,user_about_me'})
};

const facebookLogout = () => {
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.logout();
        render(false);
        window.location.reload();
      }
   });
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me?fields=id,name,about,email,gender,cover,picture.width(168).height(168)', 'GET', function(response){
          console.log(response);
          fun(response);
        });
      }
  });
};

const getUserFeed = (fun) => {
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me/feed?fields=attachments,message,story', 'GET', function(response){
          console.log(response);
          fun(response);
        });
      }
  });
};

const postFeed = (message) => {
  FB.api('/me/feed', 'POST', {message:message});
  render(true);
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
  window.location.reload()
};

const deletePost = (user,message) => {
    FB.api("/"+user+"_"+message,"delete", function (response){
        if (response && !response.error){
            window.location.reload();
        }
        else{
            window.alert("error deleting message");
            console.log(response);
        }
    });
};