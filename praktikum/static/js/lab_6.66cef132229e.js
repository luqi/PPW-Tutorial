var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]

if(typeof(Storage) !== "undefined") {
	localStorage.themes = JSON.stringify(themes);
	if (!localStorage.selectedTheme) {
		localStorage.selectedTheme = JSON.stringify(themes[3]);
	}
} else {
	alert("Browser Anda tidak support local storage");
}
var selectedTheme = JSON.parse(localStorage.selectedTheme);
$('body').css({"backgroundColor" : selectedTheme['bcgColor']});
$('.text-center').css({"color" : selectedTheme['fontColor']});


// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
  	print.value = "";
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
  } 
  else if (x == 'sin') {
  	print.value = Math.round(Math.sin(print.value)*10000) / 10000;
  }
  else if (x == 'log') {
  	print.value = Math.round(Math.log(print.value)*10000) / 10000;
  }
  else if (x == 'tan') {
  	print.value = Math.round(Math.tan(print.value)*10000) / 10000;
  }
  else {
  	if (print.value.length == 0 && (x===" * " || x===" / " || x===" - " || x===" + " || x===".")) {
  	}
  	else {
  		if (print.value.length > 3 && (x===" * " || x===" / " || x===" - " || x===" + ")) {
  			if (print.value.charAt(print.value.length-1) === " ") {
  				print.value = print.value.substring(0,print.value.length-3) + x;
  			}
  			else {
  				print.value += x;
  			}
  		}
  		else {
  			print.value += x;
  		}
  	}
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END


$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini
    // Chat Box
    $('#chat-box-enter').keypress(function(key) {
    	if (key.which==13) {
    		var pesan = $('#chat-box-enter').val();
    		var sebelumnya = $('.msg-insert').html();
    		if (pesan.length > 0) {
    			$('.msg-insert').html(sebelumnya + '<p class="msg-send">'+pesan+'</p>');
    			$('#chat-box-enter').val("");
    		}
    		key.preventDefault();
    	}
    })


    $('.my-select').select2({
    	'data': JSON.parse(localStorage.themes)
	})

    $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    	var tema = JSON.parse(localStorage.themes)[$('.my-select').val()];
    	$('body').css({"backgroundColor" : tema['bcgColor']})
    	$('.text-center').css({"color" : tema['fontColor']})
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    	localStorage.selectedTheme = JSON.stringify(tema)
	})
});