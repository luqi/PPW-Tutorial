from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.
class lab7UnitTest(TestCase):
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_get_friend_list_data_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

	def test_add_friend(self):
		response = Client().post('/lab-7/add-friend/', {'name' : "bukan Yudis", 'npm':'bukan123456'})
		self.assertEqual(response.status_code, 200)
		response = Client().get('/lab-7/')
		html_response = response.content.decode('utf8')
		self.assertIn("bukan Yudis", html_response)

	def test_delete_friend(self):
		teman = Friend(friend_name="Yudis", npm="2000")
		teman.save()
		id_teman = teman.id
		response = Client().post('/lab-7/delete-friend/', {'id':id_teman})
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(teman, Friend.objects.all())

	def test_invalid_sso(self):
		username = "Yumna"
		password = "Ancol"
		csui_helper = CSUIhelper()
		with self.assertRaises(Exception) as context:
			csui_helper.instance.get_access_token(username, password)
		self.assertIn("Yumna", str(context.exception))


	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})