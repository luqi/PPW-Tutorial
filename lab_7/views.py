from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Friend, Mahasiswa
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {"author":"Khoirul Khuluqi Abdulloh"}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    if (Mahasiswa.objects.count() == 0):
        for i in range(len(mahasiswa_list)) :
            Mahasiswa.objects.create(nama = mahasiswa_list[i]["nama"], npm = mahasiswa_list[i]["npm"])

        csui_helper.instance.next_mahasiswa_list()
        mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
        for i in range(len(mahasiswa_list)) :
            Mahasiswa.objects.create(nama = mahasiswa_list[i]["nama"], npm = mahasiswa_list[i]["npm"])

    semua_mahasiswa = Mahasiswa.objects.all()
    paginator = Paginator(semua_mahasiswa, 20)
    page = request.GET.get('page')
    try:
        mahasiswa_list = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa_list = paginator.page(1)

    friend_list = Friend.objects.all()
    response["mahasiswa_list"] = mahasiswa_list
    response["friend_list"] = friend_list
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def get_friend_list(request):
    if request.method == 'GET':
        friend_list = Friend.objects.all()
        data = serializers.serialize('json', friend_list)
        return HttpResponse(data)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        terpilih = Friend.objects.filter(npm=npm).exists()
        if (not terpilih):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
    Friend.objects.filter(pk=request.POST["id"]).delete()
    return HttpResponseRedirect('/lab-7/friend-list')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()#lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data